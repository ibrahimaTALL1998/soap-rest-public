package sn.esp.dgi.security.jwt;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sn.esp.dgi.security.web.UserDetailsServiceImpl;
 
 
@RestController
@RequestMapping("/api/authenticate")
public class LoginController {
 
	@Autowired
	private AuthenticationManager authenticationManager;
 
	@Autowired
	private UserDetailsServiceImpl customUserDetailsService;
 
	@PostMapping
	public ResponseEntity<UserTransfer> authenticate(@RequestBody AuthRequest auth) {
		try {
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(auth.getUsername(), auth.getPassword());
			Authentication authentication = this.authenticationManager.authenticate(token);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			UserDetails userDetails = this.customUserDetailsService.loadUserByUsername(auth.getUsername());
 
			List<String> roles = new ArrayList<>();
 
			for (GrantedAuthority authority : userDetails.getAuthorities()) {
				roles.add(authority.toString());
			}
 
			return new ResponseEntity<UserTransfer>(new UserTransfer(userDetails.getUsername(), roles,
					TokenUtil.createToken(userDetails), HttpStatus.OK), HttpStatus.OK);
 
		} catch (BadCredentialsException bce) {
			return new ResponseEntity<UserTransfer>(new UserTransfer(), HttpStatus.UNPROCESSABLE_ENTITY);
 
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
 
	}
}