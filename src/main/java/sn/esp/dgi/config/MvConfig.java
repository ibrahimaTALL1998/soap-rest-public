package sn.esp.dgi.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvConfig implements WebMvcConfigurer {

	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/web/article").setViewName("/web/article");
        registry.addViewController("/web/category").setViewName("/web/category");
		registry.addViewController("/login").setViewName("login");
	}

}