// package sn.esp.dgi.config;

// import javax.xml.ws.Endpoint;

// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.beans.factory.annotation.Value;
// import org.springframework.context.annotation.Bean;
// import org.springframework.context.annotation.Configuration;

// import sn.esp.dgi.web.ArticleResource;
// import sn.esp.dgi.web.CategoryResource;

// @Configuration
// public class SoapConfig {

// 	@Value("${soap.host.address}")
// 	private String soapHostAddress;

// 	@Autowired
// 	private ArticleResource articleResource;
// 	@Autowired
// 	private CategoryResource categoryResource;

// 	@Bean
// 	public Endpoint endpoint() {
// 		Endpoint.publish(soapHostAddress + "/ws/article", articleResource);
// 		return Endpoint.publish(soapHostAddress + "/ws/category", categoryResource);
// 	}

// }