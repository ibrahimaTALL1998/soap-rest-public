package sn.esp.dgi.service;

import java.util.HashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import sn.esp.dgi.domain.Role;
import sn.esp.dgi.domain.User;
import sn.esp.dgi.repository.RoleRepository;
import sn.esp.dgi.repository.UserRepository;
import sn.esp.dgi.security.web.UserPrincipal;

@Service
public class UserService implements UserDetailsService{

  @Value("${soap.admin.username}")
  private String username;

  @Value("${soap.admin.password}")
  private String password;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private RoleRepository roleRepository;

  public void initRoleAndUser() {
    if (this.userRepository.count() != 0) {
      return;
    }

    Role adminRole = new Role();
    adminRole.setName("Admin");
    adminRole.setDescription("Admin role");
    roleRepository.save(adminRole);

    Role userRole = new Role();
    userRole.setName("User");
    userRole.setDescription("Default role for newly created record");
    roleRepository.save(userRole);

    Role editeurRole = new Role();
    editeurRole.setName("Editeur");
    editeurRole.setDescription("website maintainers");
    roleRepository.save(editeurRole);

    User adminUser = new User();
    adminUser.setUsername(username);
    adminUser.setPassword(password);
    adminUser.setFirstname("admin");
    adminUser.setLastname("admin");
    Set<Role> adminRoles = new HashSet<>();
    adminRoles.add(adminRole);
    adminUser.setRole(adminRoles);
    userRepository.save(adminUser);

    User user = new User();
    user.setUsername("ibou");
    user.setPassword("ibou@123");
    user.setFirstname("ibou");
    user.setLastname("ibou");
    Set<Role> userRoles = new HashSet<>();
    userRoles.add(editeurRole);
    user.setRole(userRoles);
    userRepository.save(user);
  }

  public User registerNewUser(User user) {
    Role role = roleRepository.findById("User").get();
    Set<Role> userRoles = new HashSet<>();
    userRoles.add(role);
    user.setRole(userRoles);
    user.setPassword(user.getPassword());

    return userRepository.save(user);
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = userRepository.findByUsername(username);
    if(user==null) {
			throw new UsernameNotFoundException("User not found!");
		}		
		return new UserPrincipal(user);
  }
}
