package sn.esp.dgi.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import sn.esp.dgi.domain.Article;
import sn.esp.dgi.domain.Category;
import sn.esp.dgi.repository.ArticleRepository;

@Service
public class ArticleService {

    private Logger LOGGER = LoggerFactory.getLogger(ArticleService.class);

    @Autowired
    private ArticleRepository articleRepository;

    public Article create(Article article) {
        LOGGER.trace("Servie CREATE Article {}", article);
        return this.articleRepository.save(article);
    }

    public Article update(Article article) {
        LOGGER.trace("Servie UPDATE Article {}", article);
        return this.articleRepository.save(article);
    }

    public Article get(Integer id) {
        LOGGER.trace("Servie GET Article {}", id);
        Optional<Article> optional = this.articleRepository.findById(id);
        return optional.get();
    }

    public Page<Article> getAll(Pageable pageable) {
        LOGGER.trace("Servie GET All Article");
        return this.articleRepository.findAll(pageable);
    }

    public void delete(Integer id) {
        LOGGER.trace("Servie DELETE Article {}", id);
        this.articleRepository.deleteById(id);
    }

    public Page<Article> findByCategory(Integer id, Pageable pageable) {
        LOGGER.trace("Servie GET Article By Category {}", id);
        return this.articleRepository.findByCategory(new Category(id), pageable);
    }
}
