package sn.esp.dgi.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sn.esp.dgi.domain.Category;
import sn.esp.dgi.repository.CategoryRepository;

@Service
public class CategoryService {

    private Logger LOGGER = LoggerFactory.getLogger(CategoryRepository.class);

    @Autowired
    private CategoryRepository categoryRepository;

    public Category create(Category category) {
        LOGGER.trace("Service CREATE Category {}", category);
        return this.categoryRepository.save(category);
    }

    public Category update(Category category) {
        LOGGER.trace("Service UPDATE Category {}", category);
        return this.categoryRepository.save(category);
    }

    public Category get(Integer id) {
        LOGGER.trace("Service GET Category {}", id);
        Optional<Category> optional = this.categoryRepository.findById(id);
        return optional.get();
    }

    public List<Category> getAll() {
        LOGGER.trace("Service GET All Category");
        return this.categoryRepository.findAll();
    }

    public void delete(Integer id) {
        LOGGER.trace("Service DELETE Category {}", id);
        this.categoryRepository.deleteById(id);
    }
}