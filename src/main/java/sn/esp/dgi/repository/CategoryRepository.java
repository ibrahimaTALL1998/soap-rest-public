package sn.esp.dgi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sn.esp.dgi.domain.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
    
}
