package sn.esp.dgi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sn.esp.dgi.domain.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, String> {

}