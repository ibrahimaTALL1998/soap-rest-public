package sn.esp.dgi.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import sn.esp.dgi.domain.Article;
import sn.esp.dgi.domain.Category;

public interface ArticleRepository extends JpaRepository<Article, Integer> {
    List<Article> findByCategoryOrderByDate(Category category);
    Page<Article> findByCategory(Category category, Pageable pageable);
}
