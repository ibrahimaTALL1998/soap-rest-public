package sn.esp.dgi.controller.web;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sn.esp.dgi.domain.Article;
import sn.esp.dgi.service.ArticleService;
import sn.esp.dgi.service.CategoryService;

@Controller
@RequestMapping("/web/article")
public class ArticleController {

    @Autowired
    private ArticleService articleService;
    @Autowired
    private CategoryService categoryService;

    @GetMapping()
    public String all(@RequestParam(value = "category", required = false) Integer categoryId,
            @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "5") Integer size, Model model) {

        Pageable pageable = PageRequest.of(page, size, Sort.by("date"));
        Page<Article> response;
        if (categoryId != null) {
            response = articleService.findByCategory(categoryId, pageable);
        } else {
            response = articleService.getAll(pageable);
        }
        model.addAttribute("articles", response.getContent());
        model.addAttribute("totalPages", response.getTotalPages());
        model.addAttribute("number", response.getNumber());
        model.addAttribute("size", response.getSize());
        model.addAttribute("category", categoryId);
        model.addAttribute("categories", categoryService.getAll());

        return "article/all";
    }

    @GetMapping("/edit")
    public String edit(@RequestParam(value = "id", required = false) Integer id, Model model) {
        if (id != null) {
            model.addAttribute("article", articleService.get(id));
        } else {
            model.addAttribute("article", new Article());
        }
        model.addAttribute("categories", categoryService.getAll());
        return "article/edit";
    }

    @GetMapping("/detail/{id}")
    public String detail(@ModelAttribute("id") Integer id, Model model) {
        model.addAttribute("article", articleService.get(id));

        return "article/detail";
    }

    @GetMapping("/delete/{id}")
    public String delete(@ModelAttribute("id") Integer id) {
        articleService.delete(id);

        return "redirect:/web/article";
    }

    @PostMapping()
    public String save(@ModelAttribute("article") Article article) {
        article.setDate(new Date());
        this.articleService.create(article);
        return "redirect:/web/article";
    }
}
