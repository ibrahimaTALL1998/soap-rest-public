package sn.esp.dgi.controller.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import sn.esp.dgi.domain.Category;
import sn.esp.dgi.service.CategoryService;

@Controller
@RequestMapping("/web/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("")
    public String list(Model model) {
        Category category = new Category();
        model.addAttribute("category", category);
        List<Category> categories = categoryService.getAll();
        model.addAttribute("categories", categories);
        return "category/all";
    }

    @GetMapping("/edit/{id}")
    public String edit(Model model, @ModelAttribute("id") Integer id) {
        Category category = categoryService.get(id);
        model.addAttribute("category", category);
        List<Category> categories = categoryService.getAll();
        model.addAttribute("categories", categories);
        return "category/all";
    }

    @PostMapping()
    public String save(@ModelAttribute("category") Category category) {
        categoryService.create(category);
        return "redirect:/web/category";
    }

    @GetMapping("/create")
    public String create(Model model) {
        Category category = new Category();
        model.addAttribute("category", category);
        return "category/form";
    }

    @GetMapping("/delete/{id}")
    public String delete(@ModelAttribute("id") Integer id) {
        categoryService.delete(id);

        return "redirect:/web/category";
    }
}
