package sn.esp.dgi.controller.rest;

import java.util.List;
import java.util.Optional;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sn.esp.dgi.domain.Category;
import sn.esp.dgi.repository.CategoryRepository;

@WebService(name = "category")
@RestController
@RequestMapping("/api/category")
public class CategoryResource {

    private Logger LOGGER = LoggerFactory.getLogger(CategoryRepository.class);

    @Autowired
    private CategoryRepository categoryRepository;

    @WebMethod(operationName = "createCategory")
    @PostMapping()
    public Category createCategory(@WebParam(name = "category") @RequestBody Category category) {
        LOGGER.trace("API CREATE Category {}", category);
        return this.categoryRepository.save(category);
    }

    @WebMethod(operationName = "updateCategory")
    @PutMapping()
    public Category updateCategory(@WebParam(name = "category") @RequestBody Category category) {
        LOGGER.trace("API UPDATE Category {}", category);
        return this.categoryRepository.save(category);
    }

    @WebMethod(operationName = "getCategory")
    @GetMapping("/{id}")
    public Category getCategory(@WebParam(name = "id") @PathVariable Integer id) {
        LOGGER.trace("API GET Category {}", id);
        Optional<Category> optional = this.categoryRepository.findById(id);
        return optional.get();
    }

    @WebMethod(operationName = "getAllCategory")
    @GetMapping()
    public List<Category> getAllCategory() {
        LOGGER.trace("API GET All Category");
        return this.categoryRepository.findAll();
    }

    @WebMethod(operationName = "deleteCategory")
    @DeleteMapping("/{id}")
    public void deleteCategory(@WebParam(name = "id") @PathVariable Integer id) {
        LOGGER.trace("API DELETE Category {}", id);
        this.categoryRepository.deleteById(id);
    }
}