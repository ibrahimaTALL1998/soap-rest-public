package sn.esp.dgi.controller.rest;

import java.util.List;
import java.util.Optional;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sn.esp.dgi.domain.User;
import sn.esp.dgi.repository.UserRepository;

@WebService(name = "user")
@RestController
@RequestMapping("/api/user")
public class UserResource {

    private Logger LOGGER = LoggerFactory.getLogger(UserRepository.class);

    @Autowired
    private UserRepository userRepository;

    @WebMethod(operationName = "createUser")
    @PostMapping()
    public User createUser(@WebParam(name = "user") @RequestBody User user) {
        LOGGER.trace("API CREATE User {}", user);
        return this.userRepository.save(user);
    }

    @WebMethod(operationName = "updateUser")
    @PutMapping()
    public User updateUser(@WebParam(name = "user") @RequestBody User user) {
        LOGGER.trace("API UPDATE User {}", user);
        return this.userRepository.save(user);
    }

    @WebMethod(operationName = "getUser")
    @GetMapping("/{id}")
    public User getUser(@WebParam(name = "id") @PathVariable Integer id) {
        LOGGER.trace("API GET User {}", id);
        Optional<User> optional = this.userRepository.findById(id);
        return optional.get();
    }

    @WebMethod(operationName = "getAllUser")
    @GetMapping()
    public List<User> getAllUser() {
        LOGGER.trace("API GET All User");
        return this.userRepository.findAll();
    }

    @WebMethod(operationName = "deleteUser")
    @DeleteMapping("/{id}")
    public void deleteUser(@WebParam(name = "id") @PathVariable Integer id) {
        LOGGER.trace("API DELETE User {}", id);
        this.userRepository.deleteById(id);
    }
}