package sn.esp.dgi.controller.rest;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.springframework.stereotype.Component;

import sn.esp.dgi.domain.Article;


@WebService(name = "article")
@Component
public class SoapRestService {
        @WebMethod(exclude = true)
        public void setService() {
            System.out.println("setService...");
        }
      
        @WebMethod(operationName = "getArticle")
        public Article getArticle(@WebParam(name = "name") String name) {
            Article article = new Article();
            article.setName(name);
            return article;
        }
}