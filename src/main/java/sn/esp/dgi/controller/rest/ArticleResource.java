package sn.esp.dgi.controller.rest;

import java.util.List;
import java.util.Optional;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sn.esp.dgi.domain.Article;
import sn.esp.dgi.domain.Category;
import sn.esp.dgi.repository.ArticleRepository;

@WebService(name = "article")
@RestController
@RequestMapping("/api/article")
public class ArticleResource {

    private Logger LOGGER = LoggerFactory.getLogger(ArticleRepository.class);

    @Autowired
    private ArticleRepository articleRepository;

    @WebMethod(operationName = "createArticle")
    @PostMapping()
    public Article createArticle(@WebParam(name = "article") @RequestBody Article article) {
        LOGGER.trace("API CREATE Article {}", article);
        return this.articleRepository.save(article);
    }

    @WebMethod(operationName = "updateArticle")
    @PutMapping()
    public Article updateArticle(@WebParam(name = "article") @RequestBody Article article) {
        LOGGER.trace("API UPDATE Article {}", article);
        return this.articleRepository.save(article);
    }

    @WebMethod(operationName = "getArticle")
    @GetMapping("/{id}")
    public Article getArticle(@WebParam(name = "id") @PathVariable Integer id) {
        LOGGER.trace("API GET Article {}", id);
        Optional<Article> optional = this.articleRepository.findById(id);
        return optional.get();
    }

    @WebMethod(operationName = "getAllArticle")
    @GetMapping()
    public List<Article> getAllArticle() {
        LOGGER.trace("API GET All Article");
        return this.articleRepository.findAll(Sort.by(Sort.Direction.ASC, "date"));
    }

    @WebMethod(operationName = "deleteArticle")
    @DeleteMapping("/{id}")
    public void deleteArticle(@WebParam(name = "id") @PathVariable Integer id) {
        LOGGER.trace("API DELETE Article {}", id);
        this.articleRepository.deleteById(id);
    }

    @WebMethod(operationName = "findByCategory")
    @GetMapping("/category/{id}")
    public List<Article> findByCategory(@WebParam(name = "id") @PathVariable Integer id) {
        LOGGER.trace("API GET Article By Category {}", id);
        return this.articleRepository.findByCategoryOrderByDate(new Category(id));
    }
}