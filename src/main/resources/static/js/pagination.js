// basic paging logic to demo the buttons
var pr = document.querySelector('.paginate.left');
var pl = document.querySelector('.paginate.right');

pr.onclick = slide.bind(this, -1);
pl.onclick = slide.bind(this, 1);

var index = Number(document.querySelector('#number').innerHTML);
var total = Number(document.querySelector('#totalPages').innerHTML);
var category = document.querySelector('#category').innerHTML;
var size = document.querySelector('#size').getAttribute('value')

document.querySelector('.counter').innerHTML = (index + 1) + ' / ' + total;

pr.setAttribute('data-state', index === 0 ? 'disabled' : '');
pl.setAttribute('data-state', index === total - 1 ? 'disabled' : '');

function slide(offset) {
    index = Math.min(Math.max(index + offset, 0), total - 1);
    var cat = category ? `category=${category}&` : '';
    window.location.href = `/web/article?${cat}page=${index}&size=${size}`;
}

// slide(0);